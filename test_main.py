import unittest
from classes.SingleTest import SingleTest

from main import *


class TestMain(unittest.TestCase):
    def test(self):
        just_str = "UNIT TESTS"
        print(f'{"*"*40}\n{" "*((40-len(just_str))//2)+just_str} \n{"*"*40}')

        # # test 1 - whether file is read properly and data formatted properly
        # prep
        test1_data_read = read_source("input/test1_input.json")
        # # test 1.1 test specific "Legend" value
        SingleTest(self,
                   "Test 1.1 'Legend' value",
                   result=test1_data_read["graph"]["Legend"][0]["Title"],
                   expected="Info 1")

        # # test 1.2 test size of dictionary within "graph" read
        SingleTest(self,
                   "Test 1.2 'graph' size",
                   result=len(test1_data_read["graph"]),
                   expected=3)

        # # test 1.3 test Area->Group->Service specific value
        SingleTest(self,
                   "Test 1.3 'Area'->'Group'->'Service' name",
                   result=test1_data_read["graph"]["Areas"][0]["GroupsServices"][0]["Services"][1]["Service name"],
                   expected="Service 1.2")

        # # test 1.4 test Area->Service specific value
        SingleTest(self,
                   "Test 1.4 'Area'->'Service' 'percentage'",
                   result=test1_data_read["graph"]["Areas"][0]["GroupsServices"][2]["Percentage"],
                   expected=90)

        # # test 2 - check whether object are properly built using test data
        # prep
        test_chart = build_objects(test1_data_read)
        # # test 2.1 specific "Legend" title
        SingleTest(self,
                   "Test 2.1 - specific legend title ",
                   result=test_chart.legend.legend_items[0].title,
                   expected="Info 1")

        # # test 2.2.1 number of area objects
        SingleTest(self,
                   "Test 2.2.1 - number of areas",
                   result=len(test_chart.areas),
                   expected=1)

        # # test 2.2.2 number of group/service objects in Area
        SingleTest(self,
                   "Test 2.2.2 - number of group/service objects in Area",
                   result=len(test_chart.areas[0].groups_services),
                   expected=5)

        # # test 2.2.3 number of service objects in Group[0]
        SingleTest(self,
                   "Test 2.2.3 - number of service objects in Group[0]",
                   result=len(test_chart.areas[0].groups_services[0].services),
                   expected=2)

        # # test 2.3 test Area->Group->Service object
        SingleTest(self,
                   "Test 2.3 - 'Area'->'Group'->'Service' name",
                   result=test_chart.areas[0].groups_services[0].services[1].service_name,
                   expected="Service 1.2")

        # # test 2.4 test Area->Service object specific value
        SingleTest(self,
                   "Test 2.4 - 'Area'->'Service' 'percentage'",
                   result=test_chart.areas[0].groups_services[2].percentage,
                   expected=90)

        # # test 3 - check whether object output is generated properly
        # prep
        template_file = "input/html_temlpate.html"
        output_file = "output/test_file.html"
        output_list = generate_output(test_chart, template_file)
        # # test 3.1 test first and last output list line
        SingleTest(self,
                   "Test 3.1 - first output list line",
                   result=output_list[0] + output_list[-1],
                   expected="<!DOCTYPE html>" + "\n" + "</html>")

        # # test 3.2 test output list Title line
        SingleTest(self,
                   "Test 3.2 - test output list Title line",
                   result=output_list[4],
                   expected="    <title>Test graph</title>\n")
