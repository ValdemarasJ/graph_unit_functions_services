# imports
from classes.Structure import *
from classes.HTMLTemplates import *

import json


# # function definition
# read source files
def read_source(source_file):
    with open(source_file, 'r', encoding="utf-8") as file:
        file_data = json.load(file)
    result = file_data
    return result   # JSON


# object definition
def build_objects(data_json):

    # Legend object buildup
    legend_objects = []
    for i in data_json["graph"]["Legend"]:   # -> transform to lambda
        legend_objects.append(LegendItem(
            i["Name"], i["Title"], i["Color"]
        ))
    new_legend = Legend(legend_objects)       # TBD - build legend objects

    # Areas, groups and services object buildup
    new_areas = []        # TBD - build areas objects
    for single_area in data_json["graph"]["Areas"]:
        new_groups_services = []
        # going through each area
        # print(single_area)
        for single_group_service in single_area["GroupsServices"]:
            # for each group/service
            # check if object is service -> add to service/group list
            if single_group_service["Type"] == "service":
                # Create service -> new_groups_services
                new_groups_services.append(Service(
                    single_group_service["Service name"],
                    single_group_service["Percentage"],
                    single_group_service["Legend"]))
            # check if object is group -> iterate through each service in the group
            elif single_group_service["Type"] == "group":
                new_services = []
                for single_service in single_group_service["Services"]:
                    # check if object is service -> add to service/group list
                    new_services.append(Service(
                        single_service["Service name"],
                        single_service["Percentage"],
                        single_service["Legend"]))
                # Create group with all services inside -> new_groups_services
                new_groups_services.append(Group(
                    single_group_service["Group name"],
                    single_group_service["Legend"],
                    new_services))
        # Create new Area with all groups and services inside
        new_areas.append(Area(single_area["Name"], new_groups_services))

    # Chart object buildup
    new_chart = Chart(data_json["graph"]["Title"], new_legend, new_areas)
    return new_chart


# output generation
def generate_output(chart_object, template_file):
    # form HTMLTemplates object
    templates = HTMLTemplates()
    # read html_template
    with open(template_file, "r", encoding="utf-8") as file:
        template_lines = file.readlines()
    # break template into meaningful parts - for the moment by indexes,
    # could be updated for dynamic behaviour finding lines using regex
    result_header = template_lines[0:4]
    result_title = template_lines[4:5]
    result_styles = template_lines[5:7]
    result_content = template_lines[8:10]
    result_closing = template_lines[11:]
    # form styles
    result_styles.append(templates.form_default_styles(chart_object.legend, "input\css_default_styles.css"))
    # form title lines
    result_title[0] = f'    <title>{chart_object.title}</title>\n'
    result_content.append(templates.form_title(chart_object.title))
    # legend
    for legend in chart_object.legend.legend_items:
        result_content.append(templates.form_legend(legend.title, legend.name))
    # Areas
    last_item_was_service = True
    for area in chart_object.areas:
        # area start tag
        result_content.append(templates.form_area("start", area.area_name))
        for group_service in area.groups_services:
            if isinstance(group_service, Group):
                result_content.append(templates.form_group(group_service))
                last_item_was_service = False
            else:
                # check whether previous item was service,
                # then remove last line from result list and add new service
                if last_item_was_service:
                    result_content.pop()
                else:
                    result_content.append(templates.form_service_group(True))
                result_content.append(templates.form_service(group_service, ""))
                result_content.append(templates.form_service_group(False))
                last_item_was_service = True

        # area end tag
        result_content.append(templates.form_area("end", area.area_name))

    # write lines to output file

    result = result_header + result_title + result_styles + result_content + result_closing
    return result


# output file writing
def write_output(output_file, output_list):
    with open(output_file, "w", encoding="utf-8") as file:
        file.writelines(output_list)

# # application


if __name__ == "__main__":
    data_read = read_source("input/test1_input.json")
    print(f"Title read from file->JSON: {data_read['graph']['Title']}")

    chart = build_objects(data_read)
    print(f'Title read from object: {chart.title}')

    output = generate_output(chart, "input/html_temlpate.html")
    write_output("output/chart.html", output)
