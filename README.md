# README #

Application for quick graph generation using HTML/CSS - required organization unit provisioned services and functions from structure in JSON file.

* Input: input\input.json
* Output: output\graph.html

To minimize cluttering and number of files and ease the portability, CSS is defined within HTML file.