# overall chart structure (example, non-json):
# [ Title,
#   {Legend:
#       [info1, info2, info3]},
#   {Areas:
#       [{group1:
#           {service1, service2...}},
#       {group2:
#           {...}},
#       service3,
#       service4,
#       {group3:
#           {...}}
#       ]
#   }
#  ]


class Service:
    def __init__(self, arg_service_name, arg_percentage, arg_legend):
        self.service_name = arg_service_name
        self.percentage = arg_percentage
        self.legend_applied = arg_legend


class Group:
    def __init__(self, arg_group_name, arg_legend, arg_services=None):
        self.group_name = arg_group_name
        self.services = arg_services
        self.legend_applied = arg_legend


class Area:
    def __init__(self, arg_area_name, arg_groups_services=None):
        self.area_name = arg_area_name
        self.groups_services = arg_groups_services


class LegendItem:
    def __init__(self, arg_name, arg_title, arg_color):
        self.name = arg_name
        self.title = arg_title
        self.color = arg_color


class Legend:
    def __init__(self, arg_legend_items=None):
        self.legend_items = arg_legend_items


class Chart:
    def __init__(self, arg_title, arg_legend, arg_areas):
        self.areas = arg_areas
        self.legend = arg_legend
        self.title = arg_title

