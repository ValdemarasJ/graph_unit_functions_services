# Class that brings methods to form HTML file

class HTMLTemplates:
    def form_title(self, arg_title):
        return f"\t<H1>{arg_title}</H1>\n"

    def form_default_styles(self, legend_object, css_default_styles_file):
        default_styles = ""
        # all Legend styles
        for legend in legend_object.legend_items:
            default_styles += f"" \
                f"\t.{legend.name} {{\n" \
                f"\t\tborder-style: solid;\n" \
                f"\t\tborder-width: 1px;\n" \
                f"\t\tbackground-color: {legend.color};\n" \
                f"\t}}\n"
        # default styles from template file
        with open(css_default_styles_file, "r", encoding="utf-8") as css_template_file:
            for line in css_template_file:
                default_styles += line
        return default_styles

    def form_legend(self, arg_legend_text, legend_name):
        return f'\t<span class="legend_item"><span class="{legend_name}" class="legend_box">{"&nbsp;"*6}</span> - {arg_legend_text}</span>\n'

    def form_area(self, start_or_end, area_name):
        if start_or_end == "start":
            return f'\t<div class="area">\n' \
                   f'\t\t<h2>{area_name}</h2>\n'
        else:
            return f'\t</div>\n'

    def form_service(self, service_object, prefix):
        # config - further could be provided/read from input.json file
        red_threshold = 30
        yellow_threshold = 80
        # actions
        result = ""
        result += f'{prefix}\t\t\t<span class="service {service_object.legend_applied}">\n'
        result += f'{prefix}\t\t\t\t<span>{service_object.service_name}</span><br>\n'
        result += f'{prefix}\t\t\t\t<span class="percentage'
        # checking service percentage and assigning specific class in html
        if int(service_object.percentage) > yellow_threshold:
            result += f' perc_green'
        elif int(service_object.percentage) > red_threshold:
            result += f' perc_yellow'
        else:
            result += f' perc_red'
        result += f'">{service_object.percentage}%</span>\n'
        result += f'{prefix}\t\t\t</span>\n'
        return result

    def form_group(self, group_object):
        result = ""
        result += f'\t\t<span class="group {group_object.legend_applied}">{group_object.group_name}\n'
        prefix = "\t"
        for service in group_object.services:
            result += f'{self.form_service(service, prefix)}'
        result += f'\t\t</span>\n'
        return result

    def form_service_group(self, is_start):
        # is_start - boolean,
        #   True - return line for group start,
        #   False - return line for group end
        result = ""
        if is_start:
            result += f'\t\t<span class="service_group">\n'
        else:
            result += f'\t\t</span>\n'
        return result