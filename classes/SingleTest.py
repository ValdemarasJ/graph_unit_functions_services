# Class to improve readability in main test file, provide test output
# Tests are performed when initializing the object, arguments used:
# Singletest:
#   arg_test_case - reference to test case class (extends unittest.TestCase)
#   arg_test_name - test name - for output to console
#   arg_result - actual result that is compared to next argument - expected result
#   arg_expected - result that is expected
# Example:
# SingleTest(self, "Test 1", arg_result=test(), arg_expected=1)
#


class SingleTest:
    def __init__(self,
                 test_case,
                 test_name="Default name",
                 result=0,
                 expected=0):
        self.test_name = test_name
        self.result = result
        self.expected = expected
        print(f'{self.test_name} - test is starting')
        self.perform_assert_equal(test_case)

    def get_result(self):
        return self.result

    def get_expected(self):
        return self.expected

    def perform_assert_equal(self, test_case):
        test_case.assertEqual(self.result, self.expected)
        print("\tTest passed")
